package com.dh.fullstack.users.service.bean;

/**
 * @author Gary A. Cespedes
 **/
public class Acer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
