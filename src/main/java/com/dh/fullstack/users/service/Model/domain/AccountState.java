package com.dh.fullstack.users.service.Model.domain;

/**
 * @author Gary A. Cespedes
 **/
public enum AccountState {
    ACTIVATED,
    DEACTIVATED
}
