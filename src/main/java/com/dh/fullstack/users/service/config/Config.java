package com.dh.fullstack.users.service.config;

import com.dh.fullstack.users.service.bean.Acer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @author Gary A. Cespedes
 **/

@Configuration
public class Config {

    @Bean
    @Scope("prototype")
    public Acer beansAcer(){
        Acer acer = new Acer();
        acer.setName("I am Acer");

        return acer;
    }

}
