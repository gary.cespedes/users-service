package com.dh.fullstack.users.service.Model.domain;

import javax.persistence.*;

/**
 * @author Gary A. Cespedes
 **/

@Entity
@Table(name = "Company_Table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "companyid",
                referencedColumnName = "userid")
})
public class Company extends User{
    @Column(name = "name", nullable = false)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
